#include "mem_debug.h"
#include "mem_internals.h"
#include "mem.h"
#include "util.h"


#include <stdlib.h>
#include <stdio.h>

static bool is_Bound ( struct block_header const *fst, struct block_header const *snd )
{ return (void *) snd == (fst->contents) + (fst->capacity.bytes); }

static struct block_header *get_block_header(void *data)
{
    return ((struct block_header *) ((uint8_t *) data - offsetof(struct block_header, contents));
}






void testMalloc() {
    void *memBlock = _malloc(1000);
    struct block_header *header = get_block_header(memBlock);
    if ((header->capacity.bytes) != 1000){
        printf("\nПамять успешно выделилась\n");

    }
    _free(memBlock);
}


void testFree1() {

    void *memBlock1 = _malloc(4000);
    void *memBlock2 = _malloc(2000);

    _free(memBlock1);

    struct block_header *block_data1 = get_block_header(memBlock1);
    _free(block1);
    struct block_header *block_data2 = get_block_header(memBlock2);



    if ((block_data1->is_free == false)printf("\nОшибка при освобождении блока\n");
            else
    {
        printf("\nПри освобождении одного блока из нескольких выделенных ошибок не возникло\n");

    }

    _free(memBlock2);



}



}

void testFree2() {
    void *memBlock1 = _malloc(1000);
    void *memBlock2 = _malloc(2000);
    void *memBlock3 = _malloc(3000);


    struct block_header *block_dat3 = get_block_header(memBlock3);
    _free(block1);
    struct block_header *block_data2 = get_block_header(memBlock2);

    _free(memBlock3);
    _free(memBlock2);
    if ( ( block_data2->is_free == false ) || ( block_dat3->is_free == false ) ) {
        printf("\nПри освобождении двух блока возникла ошибка\n");
    }
    else
    {
        printf("\nПри освобождении двух блока из нескольких выделенных ошибок не возникло\n");
    }
    _free(memBlock1);

}

void testExtend() {


    void *memBlock1 = _malloc(2000);
    void *memBlock2 = _malloc(2000);
    void *memBlock3 = _malloc(2000);

    struct block_header *block1_header = get_block_header(memBlock1);
    struct block_header *block2_header = get_block_header(memBlock2);



    if (!is_Bound(block1_header, block2_header)) {
        printf("При расшерении блока возникла ошибка");
    }
    else {
        printf("\nПри расшерении блока ошибок не возникло\n");
    }
    _free(memBlock1);
    _free(memBlock2);
    _free(memBlock3);
}

void testFinal() {

    bool status = true;

    for (int i = 0; i < 15; ++i) {
        void *memBlock1 = _malloc(i);
        void *memBlock2 = _malloc(i+1);
        void *memBlock3 = _malloc(i+2);
        struct block_header *block1_header = get_block_header(memBlock1);
        struct block_header *block2_header = get_block_header(memBlock2);
        if (!blocks_continuous(block1_header, block2_header)) status = false;
        _free(memBlock1);
        _free(memBlock2);
        _free(memBlock3);
    }

    if (status)
    {
        printf("\nПоследний тест успешно пройден\n");
    }
    else {
        printf("\nПоследний тест успешно не пройден, память была выделена неверно\n");
    }

}

int main() {
    testMalloc();
    testFree1();
    testFree2();
    testExtend();
    testFinal();
}
